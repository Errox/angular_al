import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PiCalculatorComponent } from './PiCalculator/pi-calculator/pi-calculator.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { PrimeCalculatorComponent } from './PrimeCalculator/prime-calculator/prime-calculator.component';
import { HomeComponent } from './Home/home/home.component';
import { ImageLoaderComponent } from './ImageLoader/image-loader/image-loader.component';
import { TableCreatorComponent } from './TableCreator/table-creator/table-creator.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    PiCalculatorComponent,
    PrimeCalculatorComponent,
    HomeComponent,
    ImageLoaderComponent,
    TableCreatorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
