import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, of } from 'rxjs';
import { Person } from '../Models/Person.model';


@Injectable({
  providedIn: 'root'
})
export class TestDataServiceStub {

  userJson = [
    {"id":1,"first_name":"Juan","last_name":"Brosio","email":"jbrosio0@istockphoto.com","gender":"Male","ip_address":"127.85.212.25","country":"Japan"},
    {"id":2,"first_name":"Georges","last_name":"Phonix","email":"gphonix1@tinypic.com","gender":"Male","ip_address":"84.92.128.250","country":"Sweden"},
    {"id":3,"first_name":"Leopold","last_name":"Grayling","email":"lgrayling2@irs.gov","gender":"Male","ip_address":"204.34.6.244","country":"Indonesia"},
    {"id":4,"first_name":"Rori","last_name":"Railton","email":"rrailton3@berkeley.edu","gender":"Female","ip_address":"208.232.172.150","country":"Canada"},
    {"id":5,"first_name":"Sonnnie","last_name":"Butner","email":"sbutner4@ca.gov","gender":"Female","ip_address":"93.149.95.184","country":"Colombia"}
  ]

  public getData(): Observable<Person[]> {
    return of(this.userJson);
  }

}
