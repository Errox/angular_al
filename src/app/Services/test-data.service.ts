import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Person } from '../Models/Person.model';


@Injectable({
  providedIn: 'root'
})
export class TestDataService {

  baseApiUrl: string = environment.api_url;

  constructor(private httpClient: HttpClient) { }

  public getData(): Observable<Person[]> {
    return this.httpClient.get<Person[]>(`${this.baseApiUrl}/data`);
  }

}
