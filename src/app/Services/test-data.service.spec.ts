import { TestBed } from '@angular/core/testing';

import { TestDataService } from './test-data.service';
import { HttpClientModule } from '@angular/common/http';

describe('TestDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ HttpClientModule ]
  }));

  it('should be created', () => {
    const service: TestDataService = TestBed.get(TestDataService);
    expect(service).toBeTruthy();
  });
});
