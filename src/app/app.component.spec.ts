import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { PiCalculatorComponent } from './PiCalculator/pi-calculator/pi-calculator.component';
import { PrimeCalculatorComponent } from './PrimeCalculator/prime-calculator/prime-calculator.component';
import { HomeComponent } from './Home/home/home.component';
import { ImageLoaderComponent } from './ImageLoader/image-loader/image-loader.component';
import { TableCreatorComponent } from './TableCreator/table-creator/table-creator.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        PiCalculatorComponent,
        PrimeCalculatorComponent,
        HomeComponent,
        ImageLoaderComponent,
        TableCreatorComponent
      ],
      imports: [
        AppRoutingModule
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
