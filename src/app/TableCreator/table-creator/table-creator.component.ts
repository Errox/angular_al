import { Component, OnInit, AfterViewInit, Input, AfterViewChecked } from '@angular/core';
import { TestDataService } from 'src/app/Services/test-data.service';
import { Person } from 'src/app/Models/Person.model';

@Component({
  selector: 'app-table-creator',
  templateUrl: './table-creator.component.html',
  styleUrls: ['./table-creator.component.css']
})
export class TableCreatorComponent implements OnInit {

  tableTime = 0;
  startTime: Date;
  endTime: Date;

  persons: Person[];

  constructor(private testDataService: TestDataService) { }

  ngOnInit() {
  }

  getTestData(): void {
    this.persons = null;
    this.testDataService.getData().subscribe(persons => {
      this.persons = persons;
    });
  }

}
