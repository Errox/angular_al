import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { TableCreatorComponent } from './table-creator.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { TestDataService } from '../../Services/test-data.service';
import { TestDataServiceStub } from 'src/app/mocks/test-data.service.mock';
import { By } from '@angular/platform-browser';

describe('TableCreatorComponent', () => {
  let component: TableCreatorComponent;
  let fixture: ComponentFixture<TableCreatorComponent>;
  let testDataService;
  let testDataServiceSpy;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableCreatorComponent ],
      imports: [ HttpClientTestingModule ],
      providers: [ { provide: TestDataService, useClass: TestDataServiceStub }]
    }).compileComponents();


  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getting data should build the table with 5 persons', () => {

    component.getTestData();

    fixture.detectChanges();

    const tableNodes = fixture.debugElement.queryAll(By.css('.person'));

    expect(tableNodes.length).toEqual(5);

  });
});
