import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pi-calculator',
  templateUrl: './pi-calculator.component.html',
  styleUrls: ['./pi-calculator.component.css']
})
export class PiCalculatorComponent {

  piNumber = "";
  piTime = 0;
  startTime: Date;
  endTime: Date;

  calculatePi() {
    this.startTime = new Date(Date.now());
    let digits = 1000;

    digits++;


    let x = new Uint32Array(digits * 10 / 3 + 2)
    let r = new Uint32Array(digits * 10 / 3 + 2);

    let pi = new Uint32Array(digits);

    for (let j = 0; j < x.length; j++) {
      x[j] = 20;
    }

    for (let i = 0; i < digits; i++) {
      let carry = this.ToUint32(0);
      for (let j = 0; j < x.length; j++) {
        let num = this.ToUint32(x.length - j - 1);
        let dem = this.ToUint32(num * 2 + 1);

        x[j] += carry;

        let q = this.ToUint32(x[j] / dem);
        r[j] = x[j] % dem;

        carry = q * num;
      }


      pi[i] = (x[x.length - 1] / 10);


      r[x.length - 1] = x[x.length - 1] % 10;;

      for (let j = 0; j < x.length; j++) {
        x[j] = r[j] * 10;
      }
    }

    let result = "";

    let c = this.ToUint32(0);

    for (let i = pi.length - 1; i >= 0; i--) {
      pi[i] += c;
      c = pi[i] / 10;

      result = (pi[i] % 10) + result;
    }

    this.endTime = new Date(Date.now());
    this.piTime = (this.endTime.getTime() - this.startTime.getTime());

    this.piNumber = result;
  }

  ToInteger(x) {
    x = Number(x);
    return x < 0 ? Math.ceil(x) : Math.floor(x);
  }

  modulo(a, b) {
    return a - Math.floor(a / b) * b;
  }

  ToUint32(x) {
    return this.modulo(this.ToInteger(x), Math.pow(2, 32));
  }

}
