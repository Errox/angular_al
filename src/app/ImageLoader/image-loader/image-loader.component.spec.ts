import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageLoaderComponent } from './image-loader.component';
import { By } from '@angular/platform-browser';

describe('ImageLoaderComponent', () => {
  let component: ImageLoaderComponent;
  let fixture: ComponentFixture<ImageLoaderComponent>;

  let imageSources = [
    '../../../assets/1.jfif',
    '../../../assets/2.jfif',
    '../../../assets/3.jfif',
    '../../../assets/4.jfif',
    '../../../assets/5.jfif',
    '../../../assets/6.jfif',
    '../../../assets/7.jfif',
    '../../../assets/8.jfif',
    '../../../assets/9.jfif',
    '../../../assets/10.jfif'
  ]

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test', () => {

    const imageNodes = fixture.debugElement.query(By.css('.imageBox')).children;

    imageNodes.forEach(imgNode => {
      expect(imageSources).toContain(imgNode.attributes.src);
    });

    expect(imageNodes.length).toEqual(10);
  });
});
