import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { PiCalculatorComponent } from '../PiCalculator/pi-calculator/pi-calculator.component';
import { PrimeCalculatorComponent } from '../PrimeCalculator/prime-calculator/prime-calculator.component';
import { HomeComponent } from '../Home/home/home.component';
import { ImageLoaderComponent } from '../ImageLoader/image-loader/image-loader.component';
import { TableCreatorComponent } from '../TableCreator/table-creator/table-creator.component';


const routes: Routes = [
  { path: 'calculatepi', component: PiCalculatorComponent },
  { path: 'calculateprime', component: PrimeCalculatorComponent },
  { path: '',     component: HomeComponent },
  { path: 'imagesloader', component: ImageLoaderComponent },
  { path: 'tablecreator', component: TableCreatorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
