import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-prime-calculator',
  templateUrl: './prime-calculator.component.html',
  styleUrls: ['./prime-calculator.component.css']
})
export class PrimeCalculatorComponent {

  primeNumbers = "";
  primeTime = 0;
  startTime: Date;
  endTime: Date;

  calculatePrime() {
    this.primeNumbers = "";
    this.startTime = new Date(Date.now());
    let num = 20000;

    let isPrime = true;
    for (let i = 0; i <= num; i++)
    {
      for (let j = 2; j <= num; j++)
      {
        if (i !== j && i % j === 0) {
          isPrime = false;
          break;
        }
      }
      if (isPrime) {
        this.primeNumbers = this.primeNumbers + " , " + i;
      }
      isPrime = true;
    }

    this.endTime = new Date(Date.now());
    this.primeTime = (this.endTime.getTime() - this.startTime.getTime());
  }

}
